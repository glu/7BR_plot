### DATABASE file
# structure is the following
# scheme_name: {
#     poi1 : {"xs": xs_value, "mu": signal strength val (usually 1), "rel_err": rel xs uncert, "label": nice label for the poi to plot},
#     poi2 : {  .... },
#     ....
# }
#
# poi1/poi2/etc. should match the ones in the input files for other scripts, of course

xs_poi = {
    "6XS" : ["mu_ggF", "mu_VBF", "mu_WH", "mu_ZH", "mu_ttH", "mu_tH"],
    "7BR" : ["mu_BR_BB", "mu_BR_WW", "mu_BR_TAUTAU", "mu_BR_ZZ", "mu_BR_YY", "mu_BR_ZY", "mu_BR_MUMU"]
}

xs_db = {  
    
    "4XS" : {
        "mu_ggF" : {"xs": 101.599, "mu":1.0, "rel_err": [0.061513, -0.062205],  "label": "ggF + bbH" },
        "mu_VBF" : {"xs":   7.945, "mu":1.0, "rel_err": [0.026753, -0.027109], "label": "VBF" },
        "mu_VH" :  {"xs":   4.566, "mu":1.0, "rel_err": [0.029285, -0.029813], "label": "VH" },
        "mu_top" : {"xs":   1.327, "mu":1.0, "rel_err": [0.088721, -0.089827],  "label": "ttH + tH" },
    },
    
    "5XS" : {
        "mu_ggF" : {"xs": 101.599, "mu":1.0, "rel_err": [0.061513, -0.062205],  "label": "ggF + bbH" }, 
        "mu_VBF" : {"xs":   7.945, "mu":1.0, "rel_err": [0.026753, -0.027109], "label": "VBF" }, 
        "mu_WH" :  {"xs":   2.760, "mu":1.0, "rel_err": [0.027245, -0.027879], "label": "WH" }, 
        "mu_ZH" :  {"xs":   1.807, "mu":1.0, "rel_err": [0.041512, -0.041816], "label": "ZH" }, 
        "mu_top" : {"xs":   1.327, "mu":1.0, "rel_err": [0.088721, -0.089827],  "label": "ttH + tH" }, 
    },
    
    "6XS" : {
        "mu_ttH" : {"xs":   1.135, "mu":1.0, "rel_err": [0.102535, -0.102159],  "label": "ttH" },
        "mu_ZH" :  {"xs":   1.807, "mu":1.0, "rel_err": [0.041512, -0.041816], "label": "ZH" }, 
        "mu_WH" :  {"xs":   2.760, "mu":1.0, "rel_err": [0.027245, -0.027879], "label": "WH" }, 
        "mu_VBF" : {"xs":   7.945, "mu":1.0, "rel_err": [0.026753, -0.027109], "label": "VBF" },
        "mu_ggF" : {"xs": 101.599, "mu":1.0, "rel_err": [0.061513, -0.062205],  "label": "ggF" }, 
        "mu_tH" :  {"xs":   0.192, "mu":1.0, "rel_err": [0.065582, -0.128370],  "label": "tH" }
    },

    "7BR" : {
        "mu_BR_BB" : {"xs":   0.5809, "mu":1.0, "rel_err": [0.017, -0.017],  "label": "bb" },
        "mu_BR_WW" :  {"xs":   0.2152, "mu":1.0, "rel_err": [0.022, -0.021], "label": "WW" }, 
        "mu_BR_ZZ" :  {"xs":   0.02641, "mu":1.0, "rel_err": [0.022, -0.021], "label": "ZZ" }, 
        "mu_BR_YY" : {"xs":  0.00227 , "mu":1.0, "rel_err": [0.029, -0.028], "label": "#gamma#gamma" },
        "mu_BR_MUMU" : {"xs": 0.0002171, "mu":1.0, "rel_err": [0.024, -0.024],  "label": "#mu#mu" }, 
        "mu_BR_TAUTAU" : {"xs": 0.06256  , "mu":1.0, "rel_err": [0.023, -0.023],  "label": "#tau#tau" } ,
        "mu_BR_ZY" :  {"xs": 0.001541 , "mu":1.0, "rel_err": [0.068, -0.069],  "label": "Z#gamma" }   
    },

    "Stage0" : {
        "mu_gg2H" :    {"xs": 101.788, "mu":1.0, "rel_err": [0.061428, -0.062119],  "label": "gg#rightarrowH" },
        "mu_qq2Hqq" :  {"xs":  10.865, "mu":1.0, "rel_err": [0.025533, -0.025972], "label": "qq#rightarrowH qq" },
        "mu_qq2Hlnu":  {"xs":   0.915, "mu":1.0, "rel_err": [0.027388, -0.028028], "label": "qq#rightarrowH l#nu" },
        "mu_Hll" :     {"xs":   0.542, "mu":1.0, "rel_err": [0.044844, -0.045120], "label": "H ll/(#nu#nu)" },
        "mu_ttH" :     {"xs":   1.135, "mu":1.0, "rel_err": [0.102535, -0.102159],  "label": "ttH" },
        "mu_tH" :      {"xs":   0.192, "mu":1.0, "rel_err": [0.065582, -0.128370],  "label": "tH" },
    },
    
    "28XS" : {
        "mu_gg2H_0J_ptH_0_10" :                  {"xs":  15.066, "mu": 1, "rel_err": [0.130641, -0.130440], "label": "gg#rightarrowH 0J 0 < p_{T}^{H} 10"},
        "mu_gg2H_0J_ptH_gt10" :                  {"xs":  46.857, "mu": 1, "rel_err": [0.076406, -0.077120], "label": "gg#rightarrowH 0J p_{T}^{H} > 10"},
        "mu_gg2H_1J_ptH_0_60" :                  {"xs":  14.758, "mu": 1, "rel_err": [0.138320, -0.138632], "label": "gg#rightarrowH 1J 0 < p_{T}^{H} < 60"},
        "mu_gg2H_1J_ptH_60_120" :                {"xs":  10.222, "mu": 1, "rel_err": [0.140004, -0.140089], "label": "gg#rightarrowH 1J 60 < p_{T}^{H} < 120"},
        "mu_gg2H_1J_ptH_120_200" :               {"xs":   1.696, "mu": 1, "rel_err": [0.174533, -0.174417], "label": "gg#rightarrowH 1J 120 < p_{T}^{H} < 200"},
        "mu_gg2H_ge2J_mJJ_0_350_ptH_0_120" :     {"xs":   6.727, "mu": 1, "rel_err": [0.210813, -0.210904], "label": "gg#rightarrowH #geq2J 0 < m_{JJ} < 350, 0 < p_{T}^{H} < 120"},
        "mu_gg2H_ge2J_mJJ_0_350_ptH_120_200" :   {"xs":   2.141, "mu": 1, "rel_err": [0.229857, -0.229756], "label": "gg#rightarrowH #geq2J 0 < m_{JJ} < 350, 120 < p_{T}^{H} < 200"},
        "mu_gg2H_ge2J_mJJ_gt350_ptH_0_200" :     {"xs":   1.992, "mu": 1, "rel_err": [0.237808, -0.237771], "label": "gg#rightarrowH #geq2J m_{JJ} > 350, 0 < p_{T}^{H} < 200"},
        "mu_gg2H_ptH_200_300" :                  {"xs":   1.040, "mu": 1, "rel_err": [0.225555, -0.225305], "label": "gg#rightarrowH 200 < p_{T}^{H} < 300"},
        "mu_gg2H_ptH_300_450" :                  {"xs":   0.241, "mu": 1, "rel_err": [0.254046, -0.253686], "label": "gg#rightarrowH 300 < p_{T}^{H} < 450"},
        "mu_gg2H_ptH_gt450" :                    {"xs":   0.041, "mu": 1, "rel_err": [0.291487, -0.290991], "label": "gg#rightarrowH p_{T}^{H} > 450"},
        "mu_qq2Hqq_VHhad" :                      {"xs":   1.158, "mu": 1, "rel_err": [0.036934, -0.037365], "label": "qq#rightarrowHqq #leq 1J & VH Veto"},
        "mu_qq2Hqq_le1J_VHveto" :                {"xs":   6.578, "mu": 1, "rel_err": [0.028780, -0.029143], "label": "qq#rightarrowHqq #geq2J VH had"},
        "mu_qq2Hqq_ge2J_mJJ_350_700_ptH_0_200" : {"xs":   1.215, "mu": 1, "rel_err": [0.030726, -0.031012], "label": "qq#rightarrowHqq #geq2J 350 < m_{JJ} < 700, 0 < p_{T}^{H} < 200"},
        "mu_qq2Hqq_ge2J_mJJ_700_1000_ptH_0_200": {"xs":   0.581, "mu": 1, "rel_err": [0.032321, -0.032641], "label": "qq#rightarrowHqq #geq2J 700 < m_{JJ} < 1000, 0 < p_{T}^{H} < 200"},
        "mu_qq2Hqq_ge2J_mJJ_gt1000_ptH_0_200" :  {"xs":   0.999, "mu": 1, "rel_err": [0.031505, -0.031952], "label": "qq#rightarrowHqq #geq2J m_{JJ} > 1000, 0 < p_{T}^{H} < 200"},
        "mu_qq2Hqq_ge2J_mJJ_350_1000_ptH_gt200": {"xs":   0.167, "mu": 1, "rel_err": [0.029068, -0.029393], "label": "qq#rightarrowHqq #geq2J 350 < m_{JJ} < 1000, p_{T}^{H} > 200"},
        "mu_qq2Hqq_ge2J_mJJ_gt1000_ptH_gt200":   {"xs":   0.166, "mu": 1, "rel_err": [0.031211, -0.031330], "label": "qq#rightarrowHqq #geq2J m_{JJ} > 1000, p_{T}^{H} > 200"},
        "mu_qq2Hlnu_ptV_0_150" :                 {"xs":   0.793, "mu": 1, "rel_err": [0.028239, -0.028824], "label": "qq#rightarrowHl#nu 0 < p_{t}^{V} < 150"},
        "mu_qq2Hlnu_ptV_gt150" :                 {"xs":   0.121, "mu": 1, "rel_err": [0.044353, -0.044865], "label": "qq#rightarrowHl#nu p_{t}^{V} > 150"},
        "mu_Hll_ptV_0_150" :                     {"xs":   0.451, "mu": 1, "rel_err": [0.041545, -0.041834], "label": "Hll 0 < p_{t}^{V} < 150"},
        "mu_Hll_ptV_gt150" :                     {"xs":   0.092, "mu": 1, "rel_err": [0.118938, -0.119056], "label": "Hll p_{t}^{V} > 150"},
        "mu_ttH_ptH_0_60" :                      {"xs":   0.268, "mu": 1, "rel_err": [0.136278, -0.136052], "label": "ttH 0 < p_{T}^{H} < 60"},
        "mu_ttH_ptH_60_120" :                    {"xs":   0.404, "mu": 1, "rel_err": [0.112622, -0.112325], "label": "ttH 60 < p_{T}^{H} < 120"},
        "mu_ttH_ptH_120_200" :                   {"xs":   0.287, "mu": 1, "rel_err": [0.124548, -0.124234], "label": "ttH 120 < p_{T}^{H} < 200"},
        "mu_ttH_ptH_200_300" :                   {"xs":   0.119, "mu": 1, "rel_err": [0.143966, -0.143597], "label": "ttH 200 < p_{T}^{H} < 300"},
        "mu_ttH_ptH_gt300" :                     {"xs":   0.055, "mu": 1, "rel_err": [0.165145, -0.164652], "label": "ttH p_{T}^{H} > 300"},
        "mu_tH" :                                {"xs":   0.192, "mu": 1, "rel_err": [0.065582, -0.128370], "label": "tH"},
    },
    
    "33XS" : {
        "mu_gg2H_0J_ptH_0_10":                   {"xs":  15.066, "mu": 1, "rel_err": [0.130641, -0.130440], "label": "gg#rightarrowH 0J 0 < p_{T}^{H} 10"},
        "mu_gg2H_0J_ptH_gt10":                   {"xs":  46.857, "mu": 1, "rel_err": [0.076406, -0.077120], "label": "gg#rightarrowH 0J p_{T}^{H} > 10"},
        "mu_gg2H_1J_ptH_0_60":                   {"xs":  14.758, "mu": 1, "rel_err": [0.138320, -0.138632], "label": "gg#rightarrowH 1J 0 < p_{T}^{H} < 60"},
        "mu_gg2H_1J_ptH_60_120":                 {"xs":  10.222, "mu": 1, "rel_err": [0.140004, -0.140089], "label": "gg#rightarrowH 1J 60 < p_{T}^{H} < 120"},
        "mu_gg2H_1J_ptH_120_200":                {"xs":   1.696, "mu": 1, "rel_err": [0.174533, -0.174417], "label": "gg#rightarrowH 1J 120 < p_{T}^{H} < 200"},
        "mu_gg2H_ge2J_mJJ_0_350_ptH_0_60":       {"xs":   2.655, "mu": 1, "rel_err": [0.217326, -0.217486], "label": "gg#rightarrowH #geq2J 0 < m_{JJ} < 350, 0 < p_{T}^{H} < 60"},
        "mu_gg2H_ge2J_mJJ_0_350_ptH_60_120":     {"xs":   4.073, "mu": 1, "rel_err": [0.208862, -0.208899], "label": "gg#rightarrowH #geq2J 0 < m_{JJ} < 350, 60 < p_{T}^{H} < 120"},
        "mu_gg2H_ge2J_mJJ_0_350_ptH_120_200":    {"xs":   2.141, "mu": 1, "rel_err": [0.229857, -0.229756], "label": "gg#rightarrowH #geq2J 0 < m_{JJ} < 350, 120 < p_{T}^{H} < 200"},
        "mu_gg2H_ge2J_mJJ_350_700_ptH_0_200":    {"xs":   1.389, "mu": 1, "rel_err": [0.243674, -0.243662], "label": "gg#rightarrowH #geq2J 350 < m_{JJ} < 700, 0 < p_{T}^{H} < 200"},
        "mu_gg2H_ge2J_mJJ_700_1000_ptH_0_200":   {"xs":   0.327, "mu": 1, "rel_err": [0.290482, -0.290428], "label": "gg#rightarrowH #geq2J 700 < m_{JJ} < 1000, 0 < p_{T}^{H} < 200"},
        "mu_gg2H_ge2J_mJJ_gt1000_ptH_0_200":     {"xs":   0.276, "mu": 1, "rel_err": [0.297316, -0.297211], "label": "gg#rightarrowH #geq2J m_{JJ} > 1000, 0 < p_{T}^{H} < 200"},
        "mu_gg2H_ptH_200_300":                   {"xs":   1.040, "mu": 1, "rel_err": [0.225555, -0.225305], "label": "gg#rightarrowH 200 < p_{T}^{H} < 300"},
        "mu_gg2H_ptH_300_450":                   {"xs":   0.241, "mu": 1, "rel_err": [0.254046, -0.253686], "label": "gg#rightarrowH 300 < p_{T}^{H} < 450"},
        "mu_gg2H_ptH_gt450":                     {"xs":   0.041, "mu": 1, "rel_err": [0.291487, -0.290991], "label": "gg#rightarrowH p_{T}^{H} > 450"},
        "mu_qq2Hqq_le1J":                        {"xs":   4.909, "mu": 1, "rel_err": [0.032692, -0.033002], "label": "qq#rightarrowHqq #leq 1J"},
        "mu_qq2Hqq_VHhad":                       {"xs":   1.158, "mu": 1, "rel_err": [0.036934, -0.037365], "label": "qq#rightarrowHqq #geq2J VH had"},
        "mu_qq2Hqq_VHveto":                      {"xs":   1.669, "mu": 1, "rel_err": [0.031738, -0.032096], "label": "qq#rightarrowHqq #geq2J VH veto"},
        "mu_qq2Hqq_ge2J_mJJ_350_700_ptH_0_200":  {"xs":   1.215, "mu": 1, "rel_err": [0.030726, -0.031012], "label": "qq#rightarrowHqq #geq2J 350 < m_{JJ} < 700, 0 < p_{T}^{H} < 200"},
        "mu_qq2Hqq_ge2J_mJJ_700_1000_ptH_0_200": {"xs":   0.581, "mu": 1, "rel_err": [0.032321, -0.032641], "label": "qq#rightarrowHqq #geq2J 700 < m_{JJ} < 1000, 0 < p_{T}^{H} < 200"},
        "mu_qq2Hqq_ge2J_mJJ_gt1000_ptH_0_200":   {"xs":   0.999, "mu": 1, "rel_err": [0.031505, -0.031952], "label": "qq#rightarrowHqq #geq2J m_{JJ} > 1000, 0 < p_{T}^{H} < 200"},
        "mu_qq2Hqq_ge2J_mJJ_350_700_ptH_gt200":  {"xs":   0.100, "mu": 1, "rel_err": [0.029711, -0.030036], "label": "qq#rightarrowHqq #geq2J 350 < m_{JJ} < 700, p_{T}^{H} > 200"},
        "mu_qq2Hqq_ge2J_mJJ_700_1000_ptH_gt200": {"xs":   0.067, "mu": 1, "rel_err": [0.030885, -0.031179], "label": "qq#rightarrowHqq #geq2J 700 < m_{JJ} < 1000, p_{T}^{H} > 200"},
        "mu_qq2Hqq_ge2J_mJJ_gt1000_ptH_gt200":   {"xs":   0.166, "mu": 1, "rel_err": [0.031211, -0.031330], "label": "qq#rightarrowHqq #geq2J m_{JJ} > 1000, p_{T}^{H} > 200"},
        "mu_qq2Hlnu_ptV_0_150":                  {"xs":   0.793, "mu": 1, "rel_err": [0.028239, -0.028824], "label": "qq#rightarrowHl#nu 0 < p_{t}^{V} < 150"},
        "mu_qq2Hlnu_ptV_gt150":                  {"xs":   0.121, "mu": 1, "rel_err": [0.044353, -0.044865], "label": "qq#rightarrowHl#nu p_{t}^{V} > 150"},
        "mu_Hll_ptV_0_150":                      {"xs":   0.451, "mu": 1, "rel_err": [0.041545, -0.041834], "label": "Hll 0 < p_{t}^{V} < 150"},
        "mu_Hll_ptV_gt150":                      {"xs":   0.092, "mu": 1, "rel_err": [0.118938, -0.119056], "label": "Hll p_{t}^{V} > 150"},
        "mu_ttH_ptH_0_60":                       {"xs":   0.268, "mu": 1, "rel_err": [0.136278, -0.136052], "label": "ttH 0 < p_{T}^{H} < 60"},
        "mu_ttH_ptH_60_120":                     {"xs":   0.404, "mu": 1, "rel_err": [0.112622, -0.112325], "label": "ttH 60 < p_{T}^{H} < 120"},
        "mu_ttH_ptH_120_200":                    {"xs":   0.287, "mu": 1, "rel_err": [0.124548, -0.124234], "label": "ttH 120 < p_{T}^{H} < 200"},
        "mu_ttH_ptH_200_300":                    {"xs":   0.119, "mu": 1, "rel_err": [0.143966, -0.143597], "label": "ttH 200 < p_{T}^{H} < 300"},
        "mu_ttH_ptH_gt300":                      {"xs":   0.055, "mu": 1, "rel_err": [0.165145, -0.164652], "label": "ttH p_{T}^{H} > 300"},
        "mu_tH":                                 {"xs":   0.192, "mu": 1, "rel_err": [0.065582, -0.128370], "label": "tH"},
    }
}
