import ROOT
from array import array
from math import *
import numpy as np
import os

class STXSPlotter:
    """
    Class providing plotting functions that are useful e.g. for the visualisation of STXS fit results.

    Author: Philipp Windischhofer
    Date:   November 2019
    Email:  philipp.windischhofer@cern.ch
    
    Heavily modified by Davide Mungo, shamelessly stolen from VHbb people
    Email:  davide.pietro.mungo@cern.ch
    """

    @staticmethod
    def plot_STXS_xsec(plotdata, outfile, ymin_abs = 2, ymax_abs = 8000, ymin_rel = -0.49, ymax_rel = 2.49, inner_label = "", legend_header = "", atlassuffix = "Internal", color_change=[1, 2, 4], k_factor=1, n_scaledPOI = 0, ymin_rel2 = -0.49, ymax_rel2 = 2.49, large_canvas=False, truncation=None):
        """
        Generate nice plot of STXS cross sections

        Arguments:
            plotdata ... dictionaries containing the data to be plotted, in the form
                                          {'labels': [...], 'central_values': [...], 'total_unc_hi': [...],
                                           'total_unc_lo': [...], 'stat_unc_hi': [...], 'stat_unc_lo': [...],
                                           'theory_values': [...], 'theory_unc': [...]}
            outfile ... full path to the output file (including filename extension)
            ymin_abs / ymax_abs ... lower / upper y-axis limit for the upper pane showing the absolute cross sections
            ymin_rel / ymax_rel ... lower / upper y-axis limit for the lower (ratio) pane
            inner_label ... string to be placed in the interior of the plot
            legend_header ... description to be placed immediately above the legend
            atlassuffix ... label to follow "ATLAS"
            color_change ... list of index where plot color change
            k_factor ... if different from 1, scale tH prediction by k_factor
            large_canvas ... switch between small and large canvas with a boolean
            truncation ... list of indexes where to show likelihood truncation
        """

        ROOT.gStyle.SetOptStat(0)

        # have the following fields in the passed 'plotdata's:
        # labels, central_values, total_unc_hi, total_unc_lo, stat_unc_hi, stat_unc_lo, theory_values, theory_unc

        number_bins = len(plotdata["labels"])

        # create the global Canvas and the pads for absolute and relative values
        if large_canvas:  canv = ROOT.TCanvas("canv", "canv", 1600, 900)
        else:             canv = ROOT.TCanvas("canv", "canv", 800, 600)
        
        ROOT.SetOwnership(canv, False)
        canv.cd()

        if large_canvas: pads_division = 0.5
        else: pads_division = 0.35

        pad_abs = ROOT.TPad("pad_abs", "pad_abs", 0., pads_division, 1., 1.)
        ROOT.SetOwnership(pad_abs, False)
        pad_rel = ROOT.TPad("pad_rel", "pad_rel", 0., 0.01,  1., pads_division-0.001)
        ROOT.SetOwnership(pad_rel, False)

        pad_abs.Draw()
        pad_abs.SetLogy()
        pad_rel.Draw()

        pad_abs.SetTopMargin(0.05)
        pad_abs.SetRightMargin(0.1)
        pad_abs.SetLeftMargin(0.15)
        pad_abs.SetBottomMargin(0)

        pad_rel.SetTopMargin(0.0)
        pad_rel.SetRightMargin(0.1)
        pad_rel.SetLeftMargin(0.15)
        if large_canvas:
            pad_rel.SetBottomMargin(0.6)
        else:
            pad_rel.SetBottomMargin(0.15)

        # prepare the base histogram for the upper (absolute) pane that holds the axes
        pad_abs.cd()
        base_abs = ROOT.TH1F("base_abs", "base_abs", number_bins, -0.5, number_bins - 0.5)
        ROOT.SetOwnership(base_abs, False)

	base_abs.GetXaxis().SetTickLength(0)
        base_abs.GetXaxis().SetBinLabel(1, "")
        base_abs.GetXaxis().SetLabelSize(0.0)
        base_abs.GetYaxis().SetTitle("Branching fraction")
        base_abs.GetYaxis().SetLabelSize(0.07)
        #base_abs.GetYaxis().SetLabelSize(0.07 * (1-pads_division) / (pads_division-0.001-0.01))
        base_abs.GetYaxis().SetTitleSize(0.07)
        base_abs.GetYaxis().SetTitleOffset(0.8)
        base_abs.SetMaximum(ymax_abs)
        base_abs.SetMinimum(ymin_abs)
        base_abs.Draw("axis")

        # prepare the base histogram for the lower (relative) pane that holds the axes
        pad_rel.cd()
	#pad_rel.SetTicky(1)
        base_rel = ROOT.TH1F("base_rel", "base_rel", number_bins, -0.5, number_bins - 0.5)
        ROOT.SetOwnership(base_rel, False)

        base_rel.GetXaxis().SetTickLength(0)
        base_rel.GetXaxis().SetBinLabel(1,"")
        base_rel.GetYaxis().SetTitle("Ratio to SM ")
        base_rel.GetYaxis().SetLabelSize(0.07 * (1-pads_division) / (pads_division-0.001-0.01) ) # scaled by *top_padsize / bottom_padesize
        base_rel.GetYaxis().SetTitleSize(0.07 * (1-pads_division) / (pads_division-0.001-0.01) )
        base_rel.GetYaxis().SetTitleOffset(0.8 / (1-pads_division) * (pads_division-0.001-0.01) ) # (for some reason scales in the opposite way)
        #base_rel.GetYaxis().SetTitleSize(0.37)
        #base_rel.GetYaxis().SetTitleOffset(0.8) # (for some reason scales in the opposite way)
        base_rel.GetYaxis().SetNdivisions(405)
        base_rel.SetMarkerSize(0)
        base_rel.SetMaximum(ymax_rel)
        base_rel.SetMinimum(ymin_rel)

        base_rel.GetXaxis().SetLabelOffset(0.02) 
        base_rel.GetXaxis().SetLabelSize(0.09 * (1-pads_division) / (pads_division-0.001-0.01))
        # base_rel.GetXaxis().LabelsOption("v")

        for ind, bin_label in enumerate(plotdata["labels"]):
            base_rel.GetXaxis().SetBinLabel(ind + 1, bin_label)
            if large_canvas:
                base_rel.GetXaxis().ChangeLabel(ind + 1, 318)
        
        base_rel.Draw("axis")
        
        ### draw thin lines in the ratio pane at 0 and 2
        pad_rel.cd()
        # commented by glu
        #STXSPlotter._drawLine(xstart = -0.5, ystart = 0.0, xend = number_bins - 0.5 -1 , yend = 0.0, color = ROOT.kGray)
        #STXSPlotter._drawLine(xstart = -0.5, ystart = 2.0, xend = number_bins - 0.5 -1 , yend = 2.0, color = ROOT.kGray)
        # scaled tH lines
        #STXSPlotter._drawLine(xstart = number_bins - 0.5 -1, ystart = 0.0/k_factor+ 1 - 1/k_factor, xend = number_bins - 0.5, yend = 0.0/k_factor+ 1 - 1/k_factor, color = ROOT.kGray)
        #STXSPlotter._drawLine(xstart = number_bins - 0.5 -1, ystart = 2.0/k_factor+ 1 - 1/k_factor, xend = number_bins - 0.5, yend = 2.0/k_factor+ 1 - 1/k_factor, color = ROOT.kGray)


        # prepare the values that are to be plotted
        obs_values_abs = plotdata["central_values"]
        total_unc_lo_abs = plotdata["total_unc_lo"]
        total_unc_hi_abs = plotdata["total_unc_hi"]
        stat_unc_lo_abs = plotdata["stat_unc_lo"]
        stat_unc_hi_abs = plotdata["stat_unc_hi"]
        theory_values_abs = plotdata["theory_values"]
        theory_unc_hi_abs = plotdata["theory_unc_hi"]
        theory_unc_lo_abs =  list(map(abs, plotdata["theory_unc_lo"]))
        # this is done in this roundabout way to avoid picking up a dependency on numpy ...
        obs_values_rel = [cur_obs_value_abs / cur_theory_value_abs for cur_obs_value_abs, cur_theory_value_abs in zip(obs_values_abs, theory_values_abs)]
        total_unc_lo_rel = [cur_total_unc_lo_abs / cur_theory_value_abs for cur_total_unc_lo_abs, cur_theory_value_abs in zip(total_unc_lo_abs, theory_values_abs)]
        total_unc_hi_rel = [cur_total_unc_hi_abs / cur_theory_value_abs for cur_total_unc_hi_abs, cur_theory_value_abs in zip(total_unc_hi_abs, theory_values_abs)]
        stat_unc_lo_rel = [cur_stat_unc_lo_abs / cur_theory_value_abs for cur_stat_unc_lo_abs, cur_theory_value_abs in zip(stat_unc_lo_abs, theory_values_abs)]
        stat_unc_hi_rel = [cur_stat_unc_hi_abs / cur_theory_value_abs for cur_stat_unc_hi_abs, cur_theory_value_abs in zip(stat_unc_hi_abs, theory_values_abs)]
        theory_values_rel = [cur_theory_value_abs / cur_theory_value_abs for cur_theory_value_abs in theory_values_abs]
        theory_unc_hi_rel = [cur_theory_unc_abs / cur_theory_value_abs for cur_theory_unc_abs, cur_theory_value_abs in zip(theory_unc_hi_abs, theory_values_abs)]
        theory_unc_lo_rel = [cur_theory_unc_abs / cur_theory_value_abs for cur_theory_unc_abs, cur_theory_value_abs in zip(theory_unc_lo_abs, theory_values_abs)]
        # populate the upper ("absolute") pane
        objs_abs = STXSPlotter._subplot_STXS_xsec(  obs_values = obs_values_abs,
                                                    total_unc_lo = total_unc_lo_abs,
                                                    total_unc_hi = total_unc_hi_abs,
                                                    stat_unc_lo = stat_unc_lo_abs,
                                                    stat_unc_hi = stat_unc_hi_abs,
                                                    theory_values = theory_values_abs,
                                                    theory_unc_hi = theory_unc_hi_abs,
                                                    theory_unc_lo = theory_unc_lo_abs,
                                                    color_change = color_change,
                                                    pad = pad_abs,
                                                    k_factor = 1,
                                                    n_scaledPOI = 0,
                                                    ymin_rel = ymin_rel,
                                                    ymax_rel = ymax_rel,
                                                    ymin_rel2 = ymin_rel2,
                                                    ymax_rel2 = ymax_rel2,
                                                    truncation=truncation,
                                                    name = "abs")

        # populate the lower ("relative") pane
        STXSPlotter._subplot_STXS_xsec( obs_values = obs_values_rel,
                                        total_unc_lo = total_unc_lo_rel,
                                        total_unc_hi = total_unc_hi_rel,
                                        stat_unc_lo = stat_unc_lo_rel,
                                        stat_unc_hi = stat_unc_hi_rel,
                                        theory_values = theory_values_rel,
                                        theory_unc_hi = theory_unc_hi_rel,
                                        theory_unc_lo = theory_unc_lo_rel,
                                        color_change = color_change,
                                        pad = pad_rel,
                                        k_factor = k_factor,
                                        n_scaledPOI = n_scaledPOI,
                                        ymin_rel = ymin_rel,
                                        ymax_rel = ymax_rel,
                                        ymin_rel2 = ymin_rel2,
                                        ymax_rel2 = ymax_rel2,
                                        truncation=truncation,
                                        name = "rel")

        # additional stuff in case tH is needed
        if k_factor!=1:
            # line division for BR_Zy and BR_mumu
            STXSPlotter._drawLine(xstart = number_bins - 0.5 -n_scaledPOI, ystart = ymin_rel, xend = number_bins - 0.5 -n_scaledPOI, yend = ymax_rel, color = ROOT.kBlack, style = 1)
            # axis for BR_Zy and BR_mumu (hardcoding here :()
            tH_axis = ROOT.TGaxis(number_bins - 0.5, ymin_rel, number_bins - 0.5, ymax_rel, ymin_rel2 , ymax_rel2, 505 ,"+");        
            tH_axis.SetLabelSize(0.07 * (1-pads_division) / (pads_division-0.001-0.01))
            tH_axis.SetLabelFont(42)
            tH_axis.SetLabelOffset(0.017 * (1-pads_division) / (pads_division-0.001-0.01))
            tH_axis.Draw("same")

        # draw the legend and labels
        pad_abs.cd()
        if large_canvas:
            STXSPlotter._drawATLASLabel(0.18, 0.88, atlassuffix, offset=0.07)
        else:
            #STXSPlotter._drawATLASLabel(0.18, 0.88, atlassuffix, offset=0.115) # for aux. plots
            STXSPlotter._drawATLASLabel(0.55, 0.90, atlassuffix, offset=0.115) # lite. version for paper

        # updated by glu
        #STXSPlotter._drawText(0.18, 0.81, inner_label, fontsize = 0.05)    
        #STXSPlotter._drawText(0.18, 0.73, legend_header, fontsize = 0.05)
        #STXSPlotter._drawText(0.18, 0.66, "#it{p}_{SM} = 56%", fontsize = 0.05)
        # prepare some copies of the plots to be able to tune their appearance in the legend
        objs_abs["theory_hist_leg"] = objs_abs["theory_hist"].Clone()
        objs_abs["theory_hist_leg"].SetLineWidth(2)
        objs_abs["data_graph_leg"] = objs_abs["data_graph"].Clone()
        objs_abs["data_graph_leg"].SetLineWidth(2)
        objs_abs["data_graph_leg"].SetLineColor(ROOT.kGray+3)
        objs_abs["data_graph_leg"].SetMarkerColor(ROOT.kGray+3)
        objs_abs["data_statonly_graph_leg"] = objs_abs["data_statonly_graph"].Clone()
        objs_abs["data_statonly_graph_leg"].SetLineWidth(0)
        objs_abs["data_statonly_graph_leg"].SetFillColor(ROOT.kGray+1)
        objs_abs["data_systonly_graph_leg"] = objs_abs["data_systonly_graph"].Clone()
        objs_abs["data_systonly_graph_leg"].SetLineWidth(0)
        objs_abs["data_systonly_graph_leg"].SetFillColor(ROOT.kGray+1)
        
        leg = ROOT.TLegend(0.55, 0.60, 0.90, 0.85)
        ROOT.SetOwnership(leg, False)
        leg.SetTextFont(42)
        leg.SetBorderSize(1)
        if large_canvas: leg.SetTextSize(0.05)
        else: leg.SetTextSize(0.04)
        leg.SetLineColor(0)
        leg.SetLineStyle(1)
        leg.SetLineWidth(1)
        leg.SetFillColor(0)
        leg.SetFillStyle(1)
        leg.SetNColumns(2)
        leg.AddEntry(objs_abs["data_graph_leg"], "Total", "pe")
        #leg.AddEntry(objs_abs["theory_hist_leg"], "SM Expected ", "l")
        leg.AddEntry(objs_abs["data_statonly_graph_leg"], "Stat.", "f")
        leg.AddEntry(objs_abs["theory_unc_hist"], "SM Theo.", "fl")
        leg.AddEntry(objs_abs["data_systonly_graph_leg"], "Syst.", "f")
        leg.Draw()

        # switch ticks on on both sides
        pad_abs.cd()
        ROOT.gPad.SetTicky()

        # add ticks on the right vertical axis in case no tH
        if len(obs_values_rel) <= 5:
            pad_rel.cd()
            ROOT.gPad.SetTicky()

        # manually draw small ticks to separate different STXS bins
        pad_abs.cd()
        for cur_bin in range(number_bins - 1):
            STXSPlotter._drawLine(cur_bin + 0.5, ymin_abs, cur_bin + 0.5, ymin_abs * 1.3, width = 1, color = ROOT.kBlack, style = 1)

        pad_rel.cd()
        for cur_bin in range(number_bins - 1):
            STXSPlotter._drawLine(cur_bin + 0.5, ymin_rel, cur_bin + 0.5, ymin_rel + 0.05, width = 1, color = ROOT.kBlack, style = 1)
            STXSPlotter._drawLine(cur_bin + 0.5, ymax_rel, cur_bin + 0.5, ymax_rel - 0.05, width = 1, color = ROOT.kBlack, style = 1)
        
        # redraw the bounding box to make sure it is on top of everything
        pad_abs.cd()
        base_abs.Draw("axis same")
        pad_rel.cd()
        base_rel.Draw("axis same")        
        
        #os.makedirs("plots", exist_ok=True)
        #os.makedirs("plots")
	os.system("mkdir -vp plots")
        canv.SaveAs("plots/"+outfile)

    @staticmethod
    def plot_mu(plotdata, outfile, xmin=-0.999, xmax=6,  inner_label = "", legend_header = "", atlassuffix = "Internal", color_change=[1, 2, 4], k_factor=1, large_canvas=False, truncation=None):
        """
        Generate nice plot of the STXS signal strenghts
        
        Arguments:
            plotdata ... dictionary containing the information that is to be plotted, with the following
                         format: {'central_values': [...], 'stat_uncs_hi': [...], 'stat_uncs_lo': [...],
                                  'total_uncs_lo': [...], 'total_uncs_hi': [...], 'theory_central_values': [...],
                                  'theory_uncs': [...], 'labels': [...]}
            outfile ... full path to the output file (including filename extension)
            xmin/xmax ... min/max values for x-axis
            inner_label ... description showing up in the interior of the plot
            legend_header ... description to be placed immediately above the legend
            atlassuffix ... label to follow "ATLAS"
            color_change ... list of index where plot color change
            k_factor ... if different from 1, scale all the STXS prediction apart from the last one by k_factor
            large_canvas ... switch between small and large canvas with a boolean
            truncation ... list of indexes where to show likelihood truncation
        """

        ROOT.gStyle.SetOptStat(0)

        ### Add empty bins around tH
        ## this is a bad hard trick to get some more space
	print(plotdata["labels"])
        if len(plotdata["central_values"])>5:
            for t, dic_t in plotdata.items():
                if t == "labels":
                    dic_t.insert(0, "Fake")
                    dic_t.insert(2, "Fake")
                    dic_t.insert(3, "Fake")
                elif t == "theory_central_values":
                    dic_t.insert(0, -999)
                    dic_t.insert(2, -999)
                    dic_t.insert(3, -999)
                else:
                    dic_t.insert(0, 0.)
                    dic_t.insert(2, 0.)
                    dic_t.insert(3, 0.)
            
            # color_change.append(color_change[-1]+2)
            color_change.append(color_change[-1]+2)
            color_change.append(color_change[-1]+1)
            # color_change.insert(-2, tH_split-1)
        
        print(plotdata["labels"])
        print(plotdata["theory_central_values"])
        print(color_change)

        # have the following fields in 'plotdata':
        # central_values, stat_uncs_hi, stat_uncs_lo, total_uncs_hi, total_uncs_lo, theory_central_values, theory_uncs, labels
        number_mus = len(plotdata['central_values'])

        # create the global Canvas and the pads for absolute and relative values
        if large_canvas:  canv = ROOT.TCanvas("canv", "canv", 1800, 2000)
        else:             canv = ROOT.TCanvas("canv", "canv", 800, 600)
        ROOT.SetOwnership(canv, False)

        ROOT.gPad.SetTopMargin(0.02)
        ROOT.gPad.SetLeftMargin(0.13)
        ROOT.gPad.SetBottomMargin(0.15)
        ROOT.gPad.SetRightMargin(0.05)
                
        # make the base object
        if large_canvas:
            number_bins = int(number_mus * 1.2) # allow for some extra space to fit the header labels etc.
            size_reduction = 0.35
        else:
            number_bins = int(number_mus * 1.6) # as above, but more
            size_reduction = 1

        base = ROOT.TH2D("base", "", 20, xmin, xmax, number_bins, -0.5, number_bins - 0.5)
        ROOT.SetOwnership(base, False)
        base.GetXaxis().SetTitle("#sigma^{#gamma#gamma}/#sigma^{#gamma#gamma}_{SM}")
        base.GetYaxis().SetTitle("")
        base.GetXaxis().SetTitleSize(0.06*size_reduction)
        base.GetXaxis().SetLabelSize(0.05*size_reduction)
        base.GetYaxis().SetTitleSize(0.06*size_reduction)
        base.GetYaxis().SetLabelSize(0.05*size_reduction)
        base.GetYaxis().SetTickSize(0)
        base.GetYaxis().SetTickLength(0)
        # if len(plotdata["central_values"])>5:
            # base.SetAxisRange(-0.6, number_bins-0.5, "Y")
            # base.GetYaxis().SetRangeUser(-2, number_bins-0.5)

        if large_canvas:
            ROOT.gPad.SetBottomMargin(0.08)
            ROOT.gPad.SetLeftMargin(0.3)
            base.GetXaxis().SetTickLength(0.015)
            # base.GetXaxis().SetTitleOffset(0.6)

        # set labels
        for ind, cur_label in enumerate(plotdata['labels']):
            if ind in [0, 2, 3] and len(plotdata["central_values"])>5:continue
            base.GetYaxis().SetBinLabel(ind + 1, cur_label)

        ROOT.gPad.SetTickx()
        canv.cd()
        base.Draw("")
        
        # if len(plotdata["central_values"])>5:
        #     ROOT.gPad.RangeAxis(xmin, -6., xmax, number_bins-0.5)
        #     ROOT.gPad.Update()

        if k_factor!=1:
            # additional stuff in case tH is needed
            # axis for all apart tH
            comm_axis = ROOT.TGaxis(xmin, 2.6, xmax, 2.6, (xmin - 1 + 1/k_factor)*k_factor , (xmax - 1 + 1/k_factor)*k_factor, base.GetXaxis().GetNdivisions() ,"S+");
            comm_axis.SetLabelSize(0.05*size_reduction)
            comm_axis.SetLabelFont(42)
            if large_canvas:
                comm_axis.SetTickLength(0.015)
            comm_axis.Draw("same")
            # draw the SM lines
            STXSPlotter._drawLine(xstart = 1, ystart = 3, xend = 1, yend = number_mus - 0.5, style=1, color = ROOT.kGray+2)

        else:
            # draw the SM line
            STXSPlotter._drawLine(xstart = 1, ystart = -0.5, xend = 1, yend = number_mus - 0.5, style=1, color = ROOT.kGray+2)

                #### colors
                    # line          # stat          # syst  
        colors = [[ROOT.kAzure+3 , ROOT.kAzure+6  ,   ROOT.kAzure+4 ],
                  [ROOT.kOrange+3, ROOT.kOrange+6 ,   ROOT.kOrange+9],
                  [ROOT.kSpring-7, ROOT.kSpring+1 ,   ROOT.kSpring+4],
                  [ROOT.kPink-7  , ROOT.kPink+1   ,   ROOT.kPink+4],
                  [ROOT.kWhite,    ROOT.kWhite ,     ROOT.kWhite],
                  [ROOT.kOrange-3, ROOT.kYellow ,     ROOT.kOrange-2],
                  [ROOT.kWhite,    ROOT.kWhite ,     ROOT.kWhite]
                 ]
        # set the white color to transparent
        col_white = ROOT.gROOT.GetColor(ROOT.kWhite)
        col_white.SetAlpha(0);
      
        if len(plotdata['central_values'])<=6: 
            print("Removing last color")
            colors.pop(4) ## remove last colors in case you don't have to plot tH
        
        splits = ([0] + list(number_mus - np.array(color_change))[::-1] + [number_mus])
        
        for irange, prange in enumerate(list(zip(splits, splits[1:]))[::-1]):

            the_range = range(prange[0], prange[1])
            print(the_range)
            
            # prepare the graphs for the measurements and their total and statistical uncertainties
            no_unc = array('d', [0 for cur in the_range])
            
            theo_x = array('d', [plotdata['theory_central_values'][cur] for cur in the_range])
            theo_y = array('d', the_range)
            
            th_unc_x_lo = array('d', [plotdata['theory_uncs'][cur] for cur in the_range])
            th_unc_x_hi = array('d', [plotdata['theory_uncs'][cur] for cur in the_range])

            unc_y_lo = array('d', [0.4 for i in the_range])
            unc_y_hi = array('d', [0.4 for i in the_range])

            totunc_x_lo = array('d', [plotdata['total_uncs_lo'][i] for i in the_range])
            totunc_x_hi = array('d', [plotdata['total_uncs_hi'][i] for i in the_range])

            statunc_x_lo = array('d', [plotdata['stat_uncs_lo'][cur] for cur in the_range])
            statunc_x_hi = array('d', [plotdata['stat_uncs_hi'][cur] for cur in the_range])

            statunc_y_lo = array('d', [0.15 for i in the_range])
            statunc_y_hi = array('d', [0.15 for i in the_range])

            systunc_x_lo = array('d', [list(np.sqrt(np.array(plotdata['total_uncs_lo'])**2 - np.array(plotdata['stat_uncs_lo'])**2))[i] for i in the_range])
            systunc_x_hi = array('d', [list(np.sqrt(np.array(plotdata['total_uncs_hi'])**2 - np.array(plotdata['stat_uncs_hi'])**2))[i] for i in the_range])

            systunc_y_lo = array('d', [0.15 for i in the_range])
            systunc_y_hi = array('d', [0.15 for i in the_range])
            
            data_y = array('d', the_range)
            data_x = array('d', [plotdata['central_values'][i] for i in the_range])
            
            print([plotdata['central_values'][i] for i in the_range])
            
            if k_factor!=1 and irange!=len(splits)-3:
                print("Rescaling tH")
                totunc_x_lo = array('d', [i/k_factor for i in totunc_x_lo])
                totunc_x_hi = array('d', [i/k_factor for i in totunc_x_hi])
                statunc_x_lo = array('d', [i/k_factor for i in statunc_x_lo])
                statunc_x_hi = array('d', [i/k_factor for i in statunc_x_hi])
                systunc_x_lo = array('d', [i/k_factor for i in systunc_x_lo])
                systunc_x_hi = array('d', [i/k_factor for i in systunc_x_hi])
                
                th_unc_x_lo = array('d', [i/k_factor for i in th_unc_x_lo])
                th_unc_x_hi = array('d', [i/k_factor for i in th_unc_x_hi])
                
                data_x = array('d', [i/k_factor  + 1 - 1/k_factor for i in data_x])

            ROOT.gStyle.SetHatchesSpacing(0.3)
            ROOT.gStyle.SetHatchesLineWidth(1)
            theory_uncs = ROOT.TGraphAsymmErrors(len(list(the_range)), theo_x, theo_y, th_unc_x_lo, th_unc_x_hi, unc_y_lo, unc_y_hi)
            ROOT.SetOwnership(theory_uncs, False)
            theory_uncs.SetLineWidth(0)
            theory_uncs.SetFillStyle(3344)
            theory_uncs.SetFillColor(ROOT.kGray)
            theory_uncs.Draw("same 2")

            theory = ROOT.TGraphAsymmErrors(len(list(the_range)), theo_x, theo_y, no_unc, no_unc, unc_y_lo, unc_y_hi)
            ROOT.SetOwnership(theory, False)
            theory.SetLineWidth(2)
            theory.SetLineColor(ROOT.kGray+2)
            # theory.SetMarkerStyle(0)
            theory.SetMarkerSize(0)
            theory.Draw("same Z")
                        
            ROOT.gStyle.SetEndErrorSize(0)
            data_statonly = ROOT.TGraphAsymmErrors(len(list(the_range)), data_x, data_y, statunc_x_lo, statunc_x_hi, statunc_y_lo, statunc_y_hi)
            ROOT.SetOwnership(data_statonly, False)
            data_statonly.SetFillStyle(1001)
            data_statonly.SetFillColor(colors[irange][1])
            data_statonly.SetLineWidth(0)
            # data_statonly.SetLineColor(colors[irange]+3)
            data_statonly.Draw("SAMEE2")

            ROOT.gStyle.SetEndErrorSize(0)
            ROOT.gStyle.SetHatchesSpacing(0.35)
            ROOT.gStyle.SetHatchesLineWidth(1)
            data_systonly = ROOT.TGraphAsymmErrors(len(list(the_range)), data_x, data_y, systunc_x_lo, systunc_x_hi, systunc_y_lo, systunc_y_hi)
            ROOT.SetOwnership(data_systonly, False)
            data_systonly.SetFillStyle(3353)
            data_systonly.SetFillColor(colors[irange][2])
            data_systonly.SetLineWidth(0)
            # data_systonly.SetLineColor(colors[irange]+3)
            data_systonly.Draw("SAMEE2")

            ROOT.gStyle.SetEndErrorSize(10)
            data = ROOT.TGraphAsymmErrors(len(list(the_range)), data_x, data_y, totunc_x_lo, totunc_x_hi, no_unc, no_unc)
            ROOT.SetOwnership(data, False)
            data.SetMarkerColor(colors[irange][0])
            data.SetLineColor(colors[irange][0])
            data.SetMarkerStyle(8)
            data.SetMarkerSize(1.2)
            data.SetLineWidth(2)
            data.Draw("P")
            
        # draw a shaded are next to a value to represents likelihood truncation
        if truncation:
            for i in truncation:
                rev_index = number_bins-i
                xmax_box = (plotdata["central_values"][rev_index]-plotdata["total_uncs_lo"][rev_index])/k_factor +1 -1/k_factor
                box_truncation = ROOT.TBox(xmax_box-0.45, rev_index-0.4, xmax_box, rev_index+0.4);
                ROOT.SetOwnership(box_truncation, False)
                box_truncation.SetFillStyle(3005)
                box_truncation.SetFillColor(ROOT.kBlack)
                box_truncation.SetLineColor(ROOT.kBlack)
                box_truncation.SetLineWidth(1)
                box_truncation.Draw()
                STXSPlotter._drawLine(xmax_box, rev_index-0.4, xmax_box, rev_index+0.4, width = 2, style = 1)
        
        # draw labels and legend
        if large_canvas:
            # STXSPlotter._drawATLASLabel(0.32, 0.95, atlassuffix, doNDC = True, fontsize = 0.042*size_reduction, offset=0.07)
            STXSPlotter._drawText(0.93, 0.95, legend_header, fontsize = 0.042*size_reduction, alignment = 32)
            STXSPlotter._drawText(0.32, 0.93, inner_label, fontsize = 0.042*size_reduction)
        else:
            # STXSPlotter._drawATLASLabel(0.15, 0.93, atlassuffix, doNDC = True, fontsize = 0.042, offset=0.10)
            STXSPlotter._drawText(0.93, 0.93, inner_label, fontsize = 0.042, alignment = 32)
            STXSPlotter._drawText(0.15, 0.87, legend_header, fontsize = 0.042)


        objs_abs = {}
        objs_abs["theory_unc_leg"] = theory_uncs.Clone()
        objs_abs["theory_hist_leg"] = theory.Clone()
        objs_abs["theory_hist_leg"].SetLineWidth(2)
        objs_abs["data_graph_leg"] = data.Clone()
        objs_abs["data_graph_leg"].SetLineWidth(2)
        objs_abs["data_graph_leg"].SetLineColor(ROOT.kGray+3)
        objs_abs["data_graph_leg"].SetMarkerColor(ROOT.kGray+3)
        objs_abs["data_statonly_graph_leg"] = data_statonly.Clone()
        objs_abs["data_statonly_graph_leg"].SetLineWidth(0)
        objs_abs["data_statonly_graph_leg"].SetFillColor(ROOT.kGray+1)
        objs_abs["data_systonly_graph_leg"] = data_systonly.Clone()
        objs_abs["data_systonly_graph_leg"].SetLineWidth(0)
        objs_abs["data_systonly_graph_leg"].SetFillColor(ROOT.kGray+1)

        if large_canvas:
            leg = ROOT.TLegend(0.31, 0.86, 0.93, 0.91)
        else:
            leg = ROOT.TLegend(0.14, 0.74, 0.93, 0.84)
        ROOT.SetOwnership(leg, False)
        leg.SetTextFont(42)
        leg.SetTextSize(0.041*size_reduction)
        leg.SetBorderSize(0)
        leg.SetFillStyle(0)
        leg.SetNColumns(3)        
        leg.AddEntry(objs_abs["data_graph_leg"], "Obs + Tot Unc. ", "pe")
        leg.AddEntry(objs_abs["data_statonly_graph_leg"], "Stat. unc.", "f")
        leg.AddEntry(objs_abs["data_systonly_graph_leg"], "Syst. unc.", "f")
        leg.AddEntry(objs_abs["theory_hist_leg"], "SM Expected ", "l")
        leg.AddEntry(objs_abs["theory_unc_leg"], "SM Theo. unc.", "f")
        leg.Draw()

        if large_canvas:
            xlabels_start = xmin + (xmax - xmin) * 0.69
            STXSPlotter._drawText(xlabels_start, number_mus - 0.1, "           Tot.", font = 42, fontsize = 0.04*size_reduction, doNDC = False, alignment = 12)
            STXSPlotter._drawText(xlabels_start, number_mus - 0.1, "                     Stat.   Syst.  ", font = 42, fontsize = 0.04*size_reduction, doNDC = False, alignment = 12)

            for ind, (cur_central_value, cur_total_unc_hi, cur_total_unc_lo, cur_stat_unc_hi, cur_stat_unc_lo) in enumerate(zip(plotdata['central_values'], plotdata['total_uncs_hi'], plotdata['total_uncs_lo'], plotdata['stat_uncs_hi'], plotdata['stat_uncs_lo'])):
                
                if ind in [0, 2, 3]:continue
                
                cur_syst_unc_hi = m.sqrt(cur_total_unc_hi ** 2 - cur_stat_unc_hi ** 2)
                cur_syst_unc_lo = m.sqrt(cur_total_unc_lo ** 2 - cur_stat_unc_lo ** 2)

                STXSPlotter._drawText(xlabels_start, ind, "{:.2f} {{}}^{{#bf{{#plus}}{:.2f}}}_{{#bf{{#minus}}{:.2f}}}".format(cur_central_value, cur_total_unc_hi, cur_total_unc_lo), font = 42, fontsize = 0.045*size_reduction, doNDC = False, alignment = 12)
                STXSPlotter._drawText(xlabels_start + (xmax - xmin) * 0.12, ind, "  {{}}^{{#bf{{#plus}}{:.2f}}}_{{#bf{{#minus}}{:.2f}}}".format(cur_stat_unc_hi, cur_stat_unc_lo), font = 42, fontsize = 0.045*size_reduction, doNDC = False, alignment = 12)
                STXSPlotter._drawText(xlabels_start + (xmax - xmin) * 0.12, ind, "          {{}}^{{#bf{{#plus}}{:.2f}}}_{{#bf{{#minus}}{:.2f}}}".format(cur_syst_unc_hi, cur_syst_unc_lo), font = 42, fontsize = 0.045*size_reduction, doNDC = False, alignment = 12)
                STXSPlotter._drawText(xlabels_start + (xmax - xmin) * 0.12, ind, "(           )", font = 42, fontsize = 0.065*size_reduction, doNDC = False, alignment = 12)
                # STXSPlotter._drawText(xlabels_start + (xmax - xmin) * 0.25, ind - 0.02, "         ,", font = 42, fontsize = 0.045, doNDC = False, alignment = 13)

        else:
            xlabels_start = xmin + (xmax - xmin) * 0.55
            STXSPlotter._drawText(xlabels_start, number_mus - 0.2, "           Tot.", font = 42, fontsize = 0.04, doNDC = False, alignment = 12)
            STXSPlotter._drawText(xlabels_start, number_mus - 0.2, "                     Stat.   Syst.  ", font = 42, fontsize = 0.04, doNDC = False, alignment = 12)

            for ind, (cur_central_value, cur_total_unc_hi, cur_total_unc_lo, cur_stat_unc_hi, cur_stat_unc_lo) in enumerate(zip(plotdata['central_values'], plotdata['total_uncs_hi'], plotdata['total_uncs_lo'], plotdata['stat_uncs_hi'], plotdata['stat_uncs_lo'])):
                
                if ind in [0, 2, 3] and len(plotdata["central_values"])>5:continue
                
                cur_syst_unc_hi = m.sqrt(cur_total_unc_hi ** 2 - cur_stat_unc_hi ** 2)
                cur_syst_unc_lo = m.sqrt(cur_total_unc_lo ** 2 - cur_stat_unc_lo ** 2)

                STXSPlotter._drawText(xlabels_start, ind, "{:.2f} {{}}^{{#bf{{#plus}}{:.2f}}}_{{#bf{{#minus}}{:.2f}}}".format(cur_central_value, cur_total_unc_hi, cur_total_unc_lo), font = 42, fontsize = 0.045, doNDC = False, alignment = 12)
                STXSPlotter._drawText(xlabels_start + (xmax - xmin) * 0.18, ind, "  {{}}^{{#bf{{#plus}}{:.2f}}}_{{#bf{{#minus}}{:.2f}}}".format(cur_stat_unc_hi, cur_stat_unc_lo), font = 42, fontsize = 0.045, doNDC = False, alignment = 12)
                STXSPlotter._drawText(xlabels_start + (xmax - xmin) * 0.18, ind, "          {{}}^{{#bf{{#plus}}{:.2f}}}_{{#bf{{#minus}}{:.2f}}}".format(cur_syst_unc_hi, cur_syst_unc_lo), font = 42, fontsize = 0.045, doNDC = False, alignment = 12)
                STXSPlotter._drawText(xlabels_start + (xmax - xmin) * 0.18, ind, "(           )", font = 42, fontsize = 0.065, doNDC = False, alignment = 12)
                # STXSPlotter._drawText(xlabels_start + (xmax - xmin) * 0.25, ind - 0.02, "         ,", font = 42, fontsize = 0.045, doNDC = False, alignment = 13)

        base.Draw("axis same")

        #os.makedirs("plots/ratios", exist_ok=True)
        os.system("mkdir -vp plots/ratios")
        canv.SaveAs("plots/ratios/"+outfile)

    # some private helper functions
    @staticmethod
    def _drawText(x, y, text, color = ROOT.kBlack, fontsize = 0.05, font = 42, doNDC = True, alignment = 12):
        """
        Put a TLatex at a certain position.
        """
        tex = ROOT.TLatex()
        if doNDC:
            tex.SetNDC()
        ROOT.SetOwnership(tex, False)
        tex.SetTextAlign(alignment)
        tex.SetTextSize(fontsize)
        tex.SetTextFont(font)
        tex.SetTextColor(color)
        tex.DrawLatex(x, y, text)        

    @staticmethod
    def _drawATLASLabel(x, y, text, doNDC = True, fontsize = 0.07, offset = 0.08):
        """
        Draw a nice ATLAS label.
        """
        STXSPlotter._drawText(x, y, "ATLAS", fontsize = fontsize, font = 72, doNDC = doNDC)
        STXSPlotter._drawText(x + offset, y, text, fontsize = fontsize, font = 42, doNDC = doNDC)

    @staticmethod
    def _drawLine(xstart, ystart, xend, yend, width = 2, style = 7, color = ROOT.kBlack):
        """
        Draw a nice line.
        """
        line = ROOT.TLine(xstart, ystart, xend, yend)
        ROOT.SetOwnership(line, False)
        line.SetLineColor(color)
        line.SetLineWidth(width)
        line.SetLineStyle(style)
        line.Draw()

    @staticmethod
    def _subplot_STXS_xsec(obs_values, total_unc_lo, total_unc_hi, stat_unc_lo, stat_unc_hi, theory_values, theory_unc_hi, theory_unc_lo, color_change, pad, k_factor, n_scaledPOI, ymin_rel, ymax_rel, ymin_rel2, ymax_rel2, truncation, name = "abs"):
        """
        This function is called by plot_STXS_xsec and takes care of populating a single pane (either the absolute or relative one) of the plot.
        """
        pad.cd()
        number_bins = len(obs_values)
        
        # prepare the histograms for the MC prediction and -uncertainty
        theory_pred = ROOT.TH1F("theory_pred_{}".format(name), "theory_pred_{}".format(name), number_bins, -0.5, number_bins - 0.5)
        ROOT.SetOwnership(theory_pred, False)
        # theory_pred.SetMarkerSize(0)
        theory_pred.SetLineColor(ROOT.TColor.GetColor("#F81C1C"))
        theory_pred.SetLineWidth(1)

        #ROOT.gStyle.SetHatchesSpacing(0.3)
        #ROOT.gStyle.SetHatchesLineWidth(1)
        ROOT.gStyle.SetHatchesSpacing(1.5)
        ROOT.gStyle.SetHatchesLineWidth(2)
        #theory_pred_unc = ROOT.TH1F("theory_pred_unc_{}".format(name), "theory_pred_unc_{}".format(name), number_bins, -0.5, number_bins - 0.5)
        #ROOT.SetOwnership(theory_pred_unc, False)
        # theory_pred_unc.SetMarkerSize(0)
        #theory_pred_unc.SetFillStyle(1001)
        #theory_pred_unc.SetFillColor(ROOT.TColor.GetColor("#CFCFCF"))
        #theory_pred_unc.SetLineWidth(2)
    #theory_pred_unc.SetLineColor(ROOT.TColor.GetColor("#F81C1C"))

        ## make theo_unc into a TGraphAsymmErrors to draw the asymmetric error
        ROOT.gStyle.SetEndErrorSize(0)        
        theory_pred_unc = ROOT.TGraphAsymmErrors()
        ROOT.SetOwnership(theory_pred_unc, False)
        theory_pred_unc.SetFillStyle(1001)
        theory_pred_unc.SetFillColor(ROOT.TColor.GetColor("#CFCFCF"))
        theory_pred_unc.SetLineWidth(2)
        theory_pred_unc.SetLineColor(ROOT.TColor.GetColor("#F81C1C"))

        # The parameters used for the scaling from (ymin_rel, ymax_rel) to (ymin_rel2, ymax_rel2)
        sf = (ymax_rel2-ymin_rel2)/(ymax_rel-ymin_rel)
        parK = 1/sf
        parB =  (ymin_rel*ymax_rel2 - ymin_rel2*ymax_rel)/(ymax_rel2-ymin_rel2)

        for ind, (cur_theory_pred, cur_theory_pred_unc) in enumerate(zip(theory_values, theory_unc_hi)):
            cur_bin = ind + 1
            theory_pred.SetBinContent(cur_bin, cur_theory_pred)
            theory_pred.SetBinError(cur_bin, 1E-06)

            #theory_pred_unc.SetBinContent(cur_bin, cur_theory_pred)
            #theory_pred_unc.SetBinError(cur_bin, cur_theory_pred_unc)
            theory_pred_unc.SetPoint(ind, ind, cur_theory_pred)
            theory_pred_unc.SetPointError(ind, 0.5, 0.5, theory_unc_lo[ind], theory_unc_hi[ind])

            #if cur_bin == number_bins:
            if k_factor != 1 and cur_bin in [number_bins - i for i in range(n_scaledPOI)]:
                theory_pred.SetBinContent(cur_bin, cur_theory_pred*parK + parB)
                theory_pred.SetBinError(cur_bin, 1E-06)
                #theory_pred_unc.SetBinContent(cur_bin, cur_theory_pred*parK + parB)
                #theory_pred_unc.SetBinError(cur_bin, cur_theory_pred_unc/sf)
                theory_pred_unc.SetPoint(ind, ind, cur_theory_pred*parK + parB)
                theory_pred_unc.SetPointError(ind, 0.5, 0.5, theory_unc_lo[ind]/sf, theory_unc_hi[ind]/sf)

        theory_pred_unc.Draw("SAMEE2")
        theory_pred.Draw("SAMEE")

        #### colors
                    # line          # stat          # syst  
        #colors = [[ROOT.kAzure+3 , ROOT.kAzure+6  ,   ROOT.kAzure+4 ],
        #          [ROOT.kOrange+3, ROOT.kOrange+6 ,   ROOT.kOrange+9],
        #          [ROOT.kSpring-7, ROOT.kSpring+1 ,   ROOT.kSpring+4],
        #          [ROOT.kPink-7  , ROOT.kPink+1   ,   ROOT.kPink+4],
        #          [ROOT.kOrange-3, ROOT.kYellow ,     ROOT.kOrange-2]
        #         ]
        
        ## updated by glu, specify the color of each POI in hard-coding
        ### 7BR
                    # line          # stat          # syst  
        colors = [[ROOT.TColor.GetColor("#0066CC") , ROOT.TColor.GetColor("#44AAFF")  , ROOT.TColor.GetColor("#0066CC")],  # BR_BB x
                  [ROOT.TColor.GetColor("#016600") , ROOT.TColor.GetColor("#45AA00")  , ROOT.TColor.GetColor("#016600")],  # BR_WW *
                  [ROOT.TColor.GetColor("#FF0000") , ROOT.TColor.GetColor("#FF7777")  , ROOT.TColor.GetColor("#CC0000")],  # BR_TAUTAU x
                  [ROOT.TColor.GetColor("#016600") , ROOT.TColor.GetColor("#45AA00")  , ROOT.TColor.GetColor("#016600")],    # BR_ZZ *
                  [ROOT.TColor.GetColor("#009900") , ROOT.TColor.GetColor("#00DD00")  , ROOT.TColor.GetColor("#009900")],  # BR_YY +
                  [ROOT.TColor.GetColor("#009900") , ROOT.TColor.GetColor("#00DD00")  , ROOT.TColor.GetColor("#009900")],  # BR_ZY +
                  [ROOT.TColor.GetColor("#FF6666") , ROOT.TColor.GetColor("#FFAAAA")  , ROOT.TColor.GetColor("#FF6666")]  # BR_MUMU -
                 ]
        
        if len(obs_values)<=5: colors.pop(4) ## remove last colors in case you don't have to plot tH
        
        splits = [0] + color_change + [number_bins]
        
        for irange, prange in enumerate(list(zip(splits, splits[1:]))):
            
            the_range = range(prange[0], prange[1])

            # prepare the graphs for the measurements and their total and statistical uncertainties
            unc_x_lo = array('d', [0 for cur in the_range])
            unc_x_hi = array('d', [0 for cur in the_range])        

            unc_y_lo = array('d', [total_unc_lo[i] for i in the_range])
            unc_y_hi = array('d', [total_unc_hi[i] for i in the_range])

            statunc_x_lo = array('d', [0.15 for cur in the_range])
            statunc_x_hi = array('d', [0.15 for cur in the_range])        

            statunc_y_lo = array('d', [stat_unc_lo[i] for i in the_range])
            statunc_y_hi = array('d', [stat_unc_hi[i] for i in the_range])

            systunc_x_lo = array('d', [0.15 for cur in the_range])
            systunc_x_hi = array('d', [0.15 for cur in the_range])        

            systunc_y_lo = array('d', [list(np.sqrt(np.array(total_unc_lo)**2 - np.array(stat_unc_lo)**2))[i] for i in the_range])
            systunc_y_hi = array('d', [list(np.sqrt(np.array(total_unc_hi)**2 - np.array(stat_unc_hi)**2))[i] for i in the_range])
            
            data_y = array('d', [obs_values[i] for i in the_range])
            data_x = array('d', the_range)
            
            ### Scale tH by a k_factor in the relative pad
            print(irange, len(splits)-1)
            if k_factor!=1 and irange in [number_bins - i-1 for i in range(n_scaledPOI)]:
                print("Rescaling BR")
                unc_y_lo = array('d', [i/sf for i in unc_y_lo])
                unc_y_hi = array('d', [i/sf for i in unc_y_hi])
                statunc_y_lo = array('d', [i/sf for i in statunc_y_lo])
                statunc_y_hi = array('d', [i/sf for i in statunc_y_hi])
                systunc_y_lo = array('d', [i/sf for i in systunc_y_lo])
                systunc_y_hi = array('d', [i/sf for i in systunc_y_hi])
                data_y = array('d', [i*parK + parB for i in data_y])

            ROOT.gStyle.SetEndErrorSize(0)
            data_statonly = ROOT.TGraphAsymmErrors(len(list(the_range)), data_x, data_y, statunc_x_lo, statunc_x_hi, statunc_y_lo, statunc_y_hi)
            ROOT.SetOwnership(data_statonly, False)
            data_statonly.SetFillStyle(1001)
            data_statonly.SetFillColor(colors[irange][1])
            data_statonly.SetFillColorAlpha(colors[irange][1], 0.7) ## updated by glu
            data_statonly.SetLineWidth(0)
            # data_statonly.SetLineColor(colors[irange]+3)
            data_statonly.Draw("SAMEE2")

            ROOT.gStyle.SetEndErrorSize(0)
            ROOT.gStyle.SetHatchesSpacing(0.35)
            ROOT.gStyle.SetHatchesLineWidth(1)
            data_systonly = ROOT.TGraphAsymmErrors(len(list(the_range)), data_x, data_y, systunc_x_lo, systunc_x_hi, systunc_y_lo, systunc_y_hi)
            ROOT.SetOwnership(data_systonly, False)
            data_systonly.SetFillStyle(3353)
            data_systonly.SetFillColor(colors[irange][2])
            #data_systonly.SetFillColorAlpha(colors[irange][2], 0.8) ## updated by glu
            data_systonly.SetLineWidth(0)
            # data_systonly.SetLineColor(colors[irange]+3)
            data_systonly.Draw("SAMEE2")

            ROOT.gStyle.SetEndErrorSize(10)
            data = ROOT.TGraphAsymmErrors(len(list(the_range)), data_x, data_y, unc_x_lo, unc_x_hi, unc_y_lo, unc_y_hi)
            ROOT.SetOwnership(data, False)
            data.SetMarkerColor(colors[irange][0])
            data.SetLineColor(colors[irange][0])
            data.SetMarkerStyle(8)
            data.SetMarkerSize(1.2)
            data.SetLineWidth(2)
            data.Draw("P")
            
            if truncation and name=="rel":
                for i in truncation:
                    ymax_box = (obs_values[i]-total_unc_lo[i])
                    box_truncation = ROOT.TBox(i-0.4, ymax_box-0.6, i+0.4, ymax_box);
                    ROOT.SetOwnership(box_truncation, False)
                    box_truncation.SetFillStyle(3004)
                    box_truncation.SetFillColor(ROOT.kBlack)
                    box_truncation.SetLineColor(ROOT.kBlack)
                    box_truncation.SetLineWidth(1)
                    box_truncation.Draw()
                    STXSPlotter._drawLine(i-0.5, ymax_box, i+0.5, ymax_box, width = 2, style = 1)
        
        # return a dict with the created objects in case they are needed
        retdict = {"theory_hist": theory_pred, "theory_unc_hist": theory_pred_unc, "data_graph": data, "data_statonly_graph": data_statonly, "data_systonly_graph": data_systonly}
        return retdict
