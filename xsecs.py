from argparse import ArgumentParser
import glob, os
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
import yaml
from xs_database import xs_db, xs_poi

from STXSPlotter import STXSPlotter

def PlotSignalStrenghts(bd_indir, outfile, scheme):

    labels = []
    central_values = []
    stat_uncs_hi = []
    stat_uncs_lo = []
    total_uncs_hi = []
    total_uncs_lo = []
    theory_central_values = []
    theory_uncs_hi = []
    theory_uncs_lo = []
    with open(bd_indir) as f:
        results = yaml.safe_load(f)

    # unpack everything in the correct order and pass it on to the plotting routine
    for cur_POI in xs_poi[scheme]:
        dic_db = xs_db[scheme][cur_POI]
        labels.append(dic_db["label"])
        central_values.append(results[cur_POI]['val'] * dic_db['xs'] )
        stat_uncs_hi.append(results[cur_POI]['errH_stat'] * dic_db['xs'] )
        stat_uncs_lo.append(results[cur_POI]['errL_stat'] * dic_db['xs'] )
        total_uncs_hi.append(results[cur_POI]['errH'] * dic_db['xs'] )
        total_uncs_lo.append(results[cur_POI]['errL'] * dic_db['xs'] )
        theory_central_values.append(dic_db['xs'])
        theory_uncs_hi.append(dic_db['rel_err'][0] * dic_db['xs']) #### <<---- symmetrizing theoretical uncertainties
        theory_uncs_lo.append(dic_db['rel_err'][1] * dic_db['xs'])

    plotdata = {'central_values': central_values, 'stat_unc_hi': stat_uncs_hi, 'stat_unc_lo': stat_uncs_lo, 'total_unc_lo': total_uncs_lo, 'total_unc_hi': total_uncs_hi, 'theory_values': theory_central_values, 'theory_unc_hi': theory_uncs_hi, 'theory_unc_lo': theory_uncs_lo, 'labels': labels}
    
    legend_header = "m_{H} = 125.09 GeV  |y_{H}|<2.5"
    inner_label = "#sqrt{{s}}={} TeV, {} fb^{{-1}}".format(13, "36.1 - 139")
    
    if scheme == "4XS":
        STXSPlotter.plot_STXS_xsec(plotdata, outfile, ymin_abs = 0.8, ymax_abs = 1500, ymin_rel = 0.4, ymax_rel = 1.6, k_factor=1, inner_label = inner_label, legend_header = legend_header, color_change=[1, 2, 3])
    
    elif scheme == "5XS":
        STXSPlotter.plot_STXS_xsec(plotdata, outfile, ymin_abs = 0.101, ymax_abs = 1500, ymin_rel = -0.9, ymax_rel = 2.2, k_factor=1, inner_label = inner_label, legend_header = legend_header, color_change=[1, 2, 4])
    
    elif scheme == "6XS":
        STXSPlotter.plot_STXS_xsec(plotdata, outfile, ymin_abs = 0.101, ymax_abs = 1500, ymin_rel = -0.5, ymax_rel = 2.2, k_factor=5.5, inner_label = inner_label, legend_header = legend_header, color_change=[1, 2, 4, 5])

    elif scheme == "7BR":
        STXSPlotter.plot_STXS_xsec(plotdata, outfile, ymin_abs = 1.1E-04, ymax_abs = 1, ymin_rel = 0.65, ymax_rel = 1.35, k_factor=5.0, n_scaledPOI = 2, ymin_rel2 = 0.5, ymax_rel2 = 3.5, inner_label = inner_label, legend_header = legend_header, color_change=[1, 2, 3, 4, 5, 6])
 
    elif scheme == "Stage0":
        STXSPlotter.plot_STXS_xsec(plotdata, outfile, ymin_abs = 0.101, ymax_abs = 1500, ymin_rel = -0.8, ymax_rel = 2.5, k_factor=3.8, inner_label = inner_label, legend_header = legend_header, color_change=[1, 2, 4, 5])

    elif scheme == "28XS":
        STXSPlotter.plot_STXS_xsec(plotdata, outfile, ymin_abs = 0.0101, ymax_abs = 1000, ymin_rel = -1.7, ymax_rel = 3.99, k_factor=1.9, 
                                   inner_label = inner_label, legend_header = legend_header, 
                                   color_change=[11, 18, 22, 27], large_canvas=True, truncation=[20])
    elif scheme == "33XS":
        STXSPlotter.plot_STXS_xsec(plotdata, outfile, ymin_abs = 0.0101, ymax_abs = 1000, ymin_rel = -1.2, ymax_rel = 3.99, k_factor=1.9, 
                                   inner_label = inner_label, legend_header = legend_header, 
                                   color_change=[11, 18, 22, 27], large_canvas=True)
    else:
        print("Unrecognized scheme")

if __name__ == "__main__":
    ROOT.gROOT.SetBatch(True)

    parser = ArgumentParser(description = "visualise STXS signal strenghts")
    parser.add_argument("--bd_indir", action = "store", dest = "bd_indir", help = "path to directory containing the uncertainty breakdown, usually in 'plots/breakdown'")
    parser.add_argument("--outfile", action = "store", dest = "outfile", help = "path to generated plot")
    parser.add_argument("--scheme", action = "store", dest = "scheme", help = "fit scheme to be plotted")
    args = vars(parser.parse_args())

    PlotSignalStrenghts(**args)
