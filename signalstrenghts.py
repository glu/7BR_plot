from argparse import ArgumentParser
import glob, os
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
import yaml
from xs_database import xs_db, xs_poi

from STXSPlotter import STXSPlotter

def PlotSignalStrenghts(bd_indir, outfile, scheme):
    # define some global settings for the plots
    labels = []
    central_values = []
    stat_uncs_hi = []
    stat_uncs_lo = []
    total_uncs_hi = []
    total_uncs_lo = []
    theory_central_values = []
    theory_uncs = []

    with open(bd_indir) as f:
        results = yaml.safe_load(f)

    # unpack everything in the correct order and pass it on to the plotting routine
    # Use insert(0, x) to unpack in the reverse order
    for cur_POI in xs_poi[scheme]:
	dic_db = xs_db[scheme][cur_POI]
        labels.insert(0, dic_db["label"])
        central_values.insert(0, results[cur_POI]['val'])
        stat_uncs_hi.insert(0, results[cur_POI]['errH_stat'])
        stat_uncs_lo.insert(0, abs(results[cur_POI]['errL_stat']))
        total_uncs_hi.insert(0, results[cur_POI]['errH'])
        total_uncs_lo.insert(0, abs(results[cur_POI]['errL']))
        theory_central_values.insert(0, dic_db['mu'])
        theory_uncs.insert(0, dic_db['rel_err'][0] * dic_db['mu'])
    print(labels)

    plotdata = {'central_values': central_values, 'stat_uncs_hi': stat_uncs_hi, 'stat_uncs_lo': stat_uncs_lo, 'total_uncs_lo': total_uncs_lo, 'total_uncs_hi': total_uncs_hi, 'theory_central_values': theory_central_values, 'theory_uncs': theory_uncs, 'labels': labels}

    legend_header = "H #rightarrow #gamma#gamma   m_{H} = 125.09 GeV  |y_{H}|<2.5"
    inner_label = "#sqrt{{s}}={} TeV, {} fb^{{-1}}".format(13, 139)

    if scheme == "4XS":
        STXSPlotter.plot_mu(plotdata, outfile, xmin = 0.2, xmax = 3., inner_label = inner_label, legend_header = legend_header, color_change=[1, 2, 3])
    
    elif scheme == "5XS":
        STXSPlotter.plot_mu(plotdata, outfile, xmin = -0.9, xmax = 5., inner_label = inner_label, legend_header = legend_header, color_change=[1, 2, 4])
        
    elif scheme == "6XS":
        STXSPlotter.plot_mu(plotdata, outfile, xmin = -4.0, xmax = 15.0, inner_label = inner_label, legend_header = legend_header, color_change=[1, 2, 4, 5], k_factor=0.28)

    elif scheme == "Stage0":
        STXSPlotter.plot_mu(plotdata, outfile, xmin = -5.5, xmax = 17.5, inner_label = inner_label, legend_header = legend_header, color_change=[1, 2, 4, 5], k_factor=0.28)
        
    elif scheme == "28XS":
        STXSPlotter.plot_mu(plotdata, outfile, xmin=-3.5, xmax=11.5, inner_label = inner_label, legend_header = legend_header, color_change=[11, 18, 22, 27], large_canvas=True, k_factor=0.5, truncation=[27])
    
    elif scheme == "33XS":
        STXSPlotter.plot_mu(plotdata, outfile, xmin=-5.5, xmax=11.5, inner_label = inner_label, legend_header = legend_header, color_change=[14, 23, 27, 32], large_canvas=True, k_factor=0.7, truncation=[32])

    else:
        print("Unrecognized scheme")
    
if __name__ == "__main__":
    ROOT.gROOT.SetBatch(True)

    parser = ArgumentParser(description = "generate simpleMu plot")
    parser.add_argument("--bd_indir", action = "store", dest = "bd_indir", help = "path to directory containing the uncertainty breakdown, usually in 'plots/breakdown'")
    parser.add_argument("--outfile", action = "store", dest = "outfile", help = "path to generated plot")
    parser.add_argument("--scheme", action = "store", dest = "scheme", help = "fit scheme to be plotted")
    args = vars(parser.parse_args())

    PlotSignalStrenghts(**args)
